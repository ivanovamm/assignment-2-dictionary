section.data
%include 'colon.inc'
%include 'words.inc'
extern find_word

global _start

%include 'lib.inc'
%define MAX_SIZE 255


section .rodata
enter_key: db 'Enter key ', 0
found_key: db 'Found key: ', 0
not_found_key: db 'Can not find key', 10, 0
read_error_msg: db 'Can not read message', 10, 0

section .text

_start:
    mov rdi, enter_key
    mov rsi, 1
    call print_string
    sub rsp, MAX_SIZE
    mov rsi, MAX_SIZE
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .error

    mov rdi, rax
    mov rsi, last_element
    call find_word
    test rax, rax
    jz .no

    push rax
    mov rdi, found_key
    mov rsi, 1
    call print_string
    pop rax
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1
    call print_string
    call print_newline
    add rsp, MAX_SIZE
    call exit
.error:
    mov rdi, read_error_msg
    mov rsi, 2
    call print_string
    add rsp, MAX_SIZE
    call exit
.no:
    mov rdi, not_found_key
    mov rsi, 2
    call print_string
    add rsp, MAX_SIZE
    call exit
