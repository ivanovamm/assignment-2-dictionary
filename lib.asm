global string_length
global string_equals
global exit
global print_string
global read_word
global print_newline

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
 .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    xor rax, rax
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    mov rdi, 1 
    mov rax, 1 
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 10
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0x0
    mov rcx, 10
    mov rsi, 1
    .loop:
        xor rdx, rdx
        div rcx
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        inc rsi
        test rax, rax
        jz .end
        jmp .loop
    .end:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi
        add rsp, rsi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
 .print:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
	mov rax, 1
	push r8
	push r9
.loop:	
	mov r8b, byte[rdi+rcx]
	mov r9b, byte[rsi+rcx]
	cmp r8b,r9b
	jne .exit2
	cmp r8b,0
	je .exit1
	inc rcx
	jmp .loop
.exit1:
	pop r9
	pop r8
    ret
.exit2:
	xor rax, rax
	pop r9
	pop r8
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
	push 0
   	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
 .loop:
	push rcx
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	pop rcx
	cmp rax, 0x20
	je .search_space
	cmp rax, 0x9
	je .search_space
	cmp rax, 0xA
	je .search_space
	test rax, rax
	jz .end
	cmp rsi, rcx
	je .exit
	mov byte[rdi+rcx], al
	inc rcx
	jmp .loop
 .exit:
	xor rax, rax
	ret
 .search_space:
	test rcx, rcx
	je .loop
 .end:
	mov byte[rdi+rcx], 0
	mov rdx, rcx
    mov rax, rdi
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 10
    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8b, '0'
        jb .exit
        cmp r8b, '9'
        ja .exit
        sub r8b, '0'
        inc rcx
        mul r9
        add rax, r8
        jmp .loop
    .exit:
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
	cmp rdx, rcx 
	je .error
	cmp byte[rdi+rcx], 0
	je .end
	mov rax, [rdi+rcx] 
	mov [rsi+rcx], rax 
	inc rcx            
	jmp .loop
    .error:
	xor rax,rax
	ret
    .end:
	mov byte[rsi+rcx], 0
	mov rax, rcx	
    ret
