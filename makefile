NASM := nasm -f elf64 -o
LD:=ld -o
PYTNON:=python2
RM:=rm -rf

lib.o: lib.inc
       $(NASM) $@ $<

dict.o: dict.asm
        $(NASM) $@ $<

main.o: main.asm lib.o dict.o words.inc colon.inc
	$(NASM) $@ $<

program: dict.o lib.o main.o
        $(LD) program $+

.PHONY: clean

clean:
        RM *.o program
test:
        PYTNON test.py
