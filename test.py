import subprocess

input = ["a", "b", "c", "hello", "hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello"]
output = ["element1", "element2", "element3", "", ""]
error = ["", "", "", "element not found", "invalid input"]

def checker(input, output, error):
    data = input
    out = output
    er = error
    result = subprocess.run("./program", capture_output=True, input=data.encode())
    if result.stderr.decode().replace("\n", "") == er and result.stdout.decode().replace("\n", "") == out:
        return True
    return False

count = 0
for i in range (len(input)):
    if checker(input, output, error):
        print(".")
    else:
        count += 1
        print("F")

if count>0:
    print("\n" + str(count) + " errors")
else:
    print("\nOK")






