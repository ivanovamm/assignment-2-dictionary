section .text
extern string_equals
extern string_length

global find_word

%define SIZE 8

find_word:
mov r12, rsi
.loop:
    test r12, r12
    jz .no
    mov rsi, r12
    add rsi, SIZE
    call string_equals
    test rax, rax
    jnz .yes
    mov r12, [r12]
    jmp .loop
.yes:
    mov rax, rsi
    ret
.no:
    xor rax, rax,
    ret

